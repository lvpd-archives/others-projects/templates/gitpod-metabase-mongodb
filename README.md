[![Gitpod Ready-to-Master](https://img.shields.io/badge/Gitpod-Ready--to--Code--Master-blue?logo=gitpod)](https://gitpod.io/#https://gitlab.com/gitpods/analytics-platform-with-metabase-and-mongodb) 

[![Gitpod Ready-to-Develop](https://img.shields.io/badge/Gitpod-Ready--to--Code--Develop-blue?logo=gitpod)](https://gitpod.io/#https://gitlab.com/gitpods/analytics-platform-with-metabase-and-mongodb/-/tree/develop) 

# Business Intelligence workspace template
Workspace template for the implementation of a Business Intelligence tool based on a MongoDb database and the Open source tool Metabase.

## Metabase credentials
  * Login : admin@no-gitpod.com
  * Password : admin01

## Mongo express credentials
  * Login : admin
  * Password : pass

## Mongo credentials
  * None
