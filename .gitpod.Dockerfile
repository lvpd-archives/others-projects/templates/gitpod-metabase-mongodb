FROM gitpod/workspace-mongodb

USER root

# Add Mongo-Express to current location                    
RUN npm install -g mongo-express

# Add Metabase
ENV METABASE_VERSION=0.35.1
RUN mkdir -p /data/app \
    && chown gitpod:gitpod -R /data/app \
    && wget http://downloads.metabase.com/v${METABASE_VERSION}/metabase.jar \
    && mv metabase.jar /data/app \
    && chown gitpod:gitpod -R /data/app

USER gitpod

